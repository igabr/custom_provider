terraform {
  required_providers {
    myprovider = {
      source = "app.terraform.io/ivan-test/myprovider"
      version = "0.1.0"
    }
  }
}

provider "myprovider" { 
  # Configuration options 
}
