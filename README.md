terraform {
  required_providers {
    myprovider = {
      source = "app.terraform.io/ivan_test/myprovider"
      version = "0.1.0"
    }
  }
}
